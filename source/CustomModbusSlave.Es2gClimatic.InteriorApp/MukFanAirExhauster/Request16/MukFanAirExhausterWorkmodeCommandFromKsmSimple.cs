﻿namespace CustomModbusSlave.Es2gClimatic.InteriorApp.MukAirExhauster.Request16 {
	class MukFanAirExhausterWorkmodeCommandFromKsmSimple : IMukFanAirExhausterWorkmodeCommandFromKsm {
		public bool RegulatorIsWorking { get; set; }
	}
}