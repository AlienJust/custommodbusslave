﻿namespace CustomModbusSlave.Es2gClimatic.InteriorApp.MukFlapOuterAir.Request16 {
	internal interface IKsmCommandWorkmode {
		KsmCommandWorkmode ParsedValue { get; }
		int RawValue { get; }
	}
}