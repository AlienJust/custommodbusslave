﻿using System.Windows.Controls;

namespace CustomModbusSlave.Es2gClimatic.InteriorApp.MukAirExhauster.Request16 {
	/// <summary>
	/// Interaction logic for AirExhausterReply03DataView.xaml
	/// </summary>
	public partial class MukFlapAirWinterSummerRequest16DataView : UserControl {
		public MukFlapAirWinterSummerRequest16DataView() {
			InitializeComponent();
		}
	}
}
