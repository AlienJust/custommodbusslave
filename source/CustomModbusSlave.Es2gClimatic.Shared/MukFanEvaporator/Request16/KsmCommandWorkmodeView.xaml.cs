﻿using System.Windows.Controls;

namespace CustomModbusSlave.Es2gClimatic.Shared.MukFanVaporizer.Request16 {
	/// <summary>
	/// Interaction logic for MukCondenserWorkmodeCommandFromKsmView.xaml
	/// </summary>
	public partial class KsmCommandWorkmodeView : UserControl {
		public KsmCommandWorkmodeView() {
			InitializeComponent();
		}
	}
}
