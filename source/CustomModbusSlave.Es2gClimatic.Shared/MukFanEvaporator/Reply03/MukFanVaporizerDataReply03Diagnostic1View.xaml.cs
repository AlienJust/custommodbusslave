﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CustomModbusSlave.Es2gClimatic.Shared.MukFanEvaporator.Reply03 {
	/// <summary>
	/// Interaction logic for MukFanVaporizerDataReply03Diagnostic1View.xaml
	/// </summary>
	public partial class MukFanVaporizerDataReply03Diagnostic1View : UserControl {
		public MukFanVaporizerDataReply03Diagnostic1View() {
			InitializeComponent();
		}
	}
}
