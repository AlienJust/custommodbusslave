namespace CustomModbusSlave.Es2gClimatic.CabinApp.MukFlap.Request16 {
	internal enum KsmCommandWorkmode {
		/// <summary>
		/// ���������
		/// </summary>
		Off = 0,

		/// <summary>
		/// ������ � �������������� ������
		/// </summary>
		Auto = 1,

		/// <summary>
		/// ������ �� ������ �������
		/// </summary>
		Manual = 2,

		/// <summary>
		/// ����������� ��������
		/// </summary>
		Unknown
	}
}